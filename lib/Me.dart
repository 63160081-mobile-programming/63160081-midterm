import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

void main() {
  runApp(MeApp());
}

class MeApp extends StatefulWidget {
  const MeApp({super.key});
  @override
  State<MeApp> createState() => _MeAppState();
}

class _MeAppState extends State<MeApp> {
  int _selectedIndex = 0;

  static const List<Widget> _widgetOptions = <Widget>[];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          scaffoldBackgroundColor: Color.fromARGB(161, 223, 222, 222)),
      home: Scaffold(
        body: buildBody1Widget(),
      ),
    );
  }

  Widget buildBody1Widget() {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 10),
              //Height constraint at Container widget level
              height: 30,
              child: Text(
                "ยินดีต้อนรับ",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
              child: CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://cdn-icons-png.flaticon.com/512/3048/3048127.png'),radius: 50,),
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
              child: Text(
                "63160081 : นายศุภกร อัตตโน : คณะวิทยาการสารสนเทศ",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5, left: 5, right: 5),
              child: Text(
                "หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5, left: 5, right: 5),
              child: Text(
                "สถานภาพ: กำลังศึกษา",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5, left: 5, right: 5),
              child: Text(
                "อ. ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            MyActionItems(),
            Divider(
              color: Colors.black,
            ),
            Container(
              margin: const EdgeInsets.only(top: 5, left: 5, right: 5),
              child: Text(
                "ตารางเรียน",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 0, 8, 162)),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 5, right: 5, bottom: 1),
              child: Image.asset('assets/1.2.png'),
              // height: 300,
            ),
            Container(
              margin: const EdgeInsets.only(top: 1,left: 5, right: 5),
              child: Image.asset('assets/1.3.png'),
              // height: 300,
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              margin: const EdgeInsets.only(top: 5, left: 5, right: 5),
              child: Text(
                "ผลการลงทะเบียน",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 255, 146, 14)),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 1,left: 5, right: 5),
              child: Image.asset('assets/1.5.png'),
              // height: 300,
            ),
            Divider(
              color: Colors.black,
            ),
          ],
        ),
      ],
    );
  }
  Widget RegisterListTile(){
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.app_registration,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("ลงทะเบียน"),
      ],
    );
  }
  Widget ResultListTile(){
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.description,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("ผลการศึกษา"),
      ],
    );
  }

  Widget History(){
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.account_circle,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("ประวัตินิสิต"),
      ],
    );
  }

  Widget Financial(){
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.account_balance,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("ภาระค่าใช้จ่าย"),
      ],
    );
  }

  Widget MyActionItems(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RegisterListTile(),
        ResultListTile(),
         History(),
         Financial()
      ],
    );
  }

}
