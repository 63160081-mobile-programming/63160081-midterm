import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

void main() {
  runApp(Setting());
}

class Setting extends StatefulWidget {
  const Setting({super.key});
  @override
  State<Setting> createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  int _selectedIndex = 0;

  static const List<Widget> _widgetOptions = <Widget>[];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          scaffoldBackgroundColor: Color.fromARGB(161, 223, 222, 222)),
      home: SettingsList(
      sections: [
        SettingsSection(
          title: Text('ตั้งค่า'),
          tiles: <SettingsTile>[
            SettingsTile.navigation(
              leading: Icon(Icons.account_box),
              title: Text('ตั้งค่าบัญชีผู้ใช้'),
            ),
            SettingsTile.navigation(
              leading: Icon(Icons.language),
              title: Text('ภาษา'),
              value: Text('ภาษาไทย'),
            ),
            SettingsTile.switchTile(
              onToggle: (value) {},
              initialValue: true,
              leading: Icon(Icons.notifications),
              title: Text('เปิดการแจ้งเตือน'),
            ),SettingsTile.navigation(
              leading: Icon(Icons.info_outline),
              title: Text('เกี่ยวกับ'),
            ),SettingsTile.navigation(
              leading: Icon(Icons.logout),
              title: Text('ออกจากระบบ'),
            ),
          ],
        ),
      ],
    ),
    );
  }
}