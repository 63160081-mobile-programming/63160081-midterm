import 'package:flutter/material.dart';

void main() {
  runApp(HomeApp());
}

class HomeApp extends StatefulWidget {
  const HomeApp({super.key});
  @override
  State<HomeApp> createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  int _selectedIndex = 0;

  static const List<Widget> _widgetOptions = <Widget>[];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          scaffoldBackgroundColor: Color.fromARGB(161, 223, 222, 222)),
      home: Scaffold(
        body: buildBody1Widget(),
      ),
    );
  }

  Widget buildBody1Widget() {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 10),
              //Height constraint at Container widget level
              height: 30,
              child: Text(
                "ระบบบริการศึกษา มหาวิทยาลัยบูรพา",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
              child: Text(
                "1.	การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565(ด่วน)",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
              child: Image.asset('assets/1.png'),
              height: 200,
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
              child: Text(
                "2.	กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5),
              child: Image.asset('assets/2.jpg'),
              height: 200,
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
              child: Text(
                "4.	แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5),
              child: Image.asset('assets/4.jpg'),
              height: 200,
            ),
          ],
        ),
      ],
    );
  }
}
