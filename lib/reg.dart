import 'package:example_template/Home.dart';
import 'package:example_template/Me.dart';
import 'package:example_template/setting.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(RegApp());
}

class RegApp extends StatefulWidget {
  const RegApp({super.key});
  @override
  State<RegApp> createState() => _RegAppState();
}

class _RegAppState extends State<RegApp> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    HomeApp(),
    MeApp(),
    Setting(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          scaffoldBackgroundColor: Color.fromARGB(255, 255, 255, 255)),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: buildBottomAppBarWidget(),
      ),
    );
  }

  buildAppBarWidget() {
    return AppBar(
      backgroundColor: Color.fromARGB(208, 251, 242, 63),
      leading: Container(
        padding: EdgeInsets.only(left: 12),
        child: CircleAvatar(
            backgroundImage: NetworkImage(
                'https://cdn-icons-png.flaticon.com/512/3048/3048127.png')),
      ),
      actions: [
        Container(
          padding: EdgeInsets.only(right: 20),
          child: Icon(
            Icons.search,
            color: Colors.black,
            size: 30.0,
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 20),
          child: Icon(
            Icons.notifications,
            color: Colors.black,
            size: 30.0,
          ),
        ),
      ],
    );
  }

  buildBottomAppBarWidget() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'หน้าจอหลัก',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'ผู้ใช้',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          label: 'เมนู',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Color.fromARGB(255, 0, 221, 255),
      onTap: _onItemTapped,
    );
  }

  Widget buildDrawerWidget() {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 15, 65, 107),
            ),
            child: Text('Drawer Header'),
          ),
          ListTile(
            leading: Icon(
              Icons.home,
            ),
            title: const Text('Page 1'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.train,
            ),
            title: const Text('Page 2'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
